print("Today is a good day to learn python")
print('python is fun')
print("python's string are easy to use")
print('we can even include "quotes" in strings')

# Concatenation
print("hello" + "world")

greeting = "hello"
# to take input from keyboard use input function
name = input('enter your name ')

print(greeting + " " + name)
